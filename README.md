# Spletni barvni stroboskop

1\. domača naloga pri predmetu [Osnove informacijskih sistemov](https://ucilnica.fri.uni-lj.si/course/view.php?id=54) (navodila)


## 6.1 Opis naloge in navodila

Na Bitbucket je na voljo javni repozitorij [https://bitbucket.org/asistent/stroboskop](https://bitbucket.org/asistent/stroboskop), ki vsebuje nedelujoč spletni stroboskop. V okviru domače naloge ustvarite kopijo repozitorija ter popravite in dopolnite obstoječo implementacijo spletne strani tako, da bo končna aplikacija z vsemi delujočimi funkcionalnostimi izgledala kot na spodnji sliki. Natančna navodila si preberite na [spletni učilnici](https://ucilnica.fri.uni-lj.si/mod/workshop/view.php?id=19181).

![Stroboskop](stroboskop.gif)

## 6.2 Vzpostavitev repozitorija

Naloga 6.2.2:

```
git clone https://BootySpanker@bitbucket.org/BootySpanker/stroboskop.git
```

Naloga 6.2.3:
https://bitbucket.org/BootySpanker/stroboskop/commits/4c2813382a9dcaa2c7e3e01d518397133e2f6e73

## 6.3 Skladnja strani in stilska preobrazba

Naloga 6.3.1:
https://bitbucket.org/BootySpanker/stroboskop/commits/f446e80689073710c71d76c425e3f2dce2bb413c

Naloga 6.3.2:
https://bitbucket.org/BootySpanker/stroboskop/commits/3f829262cc753cc73860b3408f0036ccc1e61e47

Naloga 6.3.3:
https://bitbucket.org/BootySpanker/stroboskop/commits/03b244d2e6cd34b59f2d082643552db9f15da266

Naloga 6.3.4:
https://bitbucket.org/BootySpanker/stroboskop/commits/5e19f61019f62ed569e4e7d2ded888ce3d881aed

Naloga 6.3.5:

```
git checkout master
git merge izgled
git push origin master
```

## 6.4 Dinamika in animacija na strani

Naloga 6.4.1:
https://bitbucket.org/BootySpanker/stroboskop/commits/e9a57ca41262e1ae776eb0fef0423483f6e778d9

Naloga 6.4.2:
https://bitbucket.org/BootySpanker/stroboskop/commits/f68ab4e79a97057366cc80f94f091c65e4aa996c

Naloga 6.4.3:
https://bitbucket.org/BootySpanker/stroboskop/commits/27fa6e3e32133561760f79a96ed8e94f513b4d1d

Naloga 6.4.4:
https://bitbucket.org/BootySpanker/stroboskop/commits/ec2cd3794f30262813a5f0e5d7f11d22c3cffeae